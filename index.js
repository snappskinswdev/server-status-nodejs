var sys = require('sys');
var exec = require('child_process').exec;


exports.getStatistic = function(callback) {
    var data = {};

    exec("grep 'cpu ' /proc/stat | awk '{usage=($2+$4)*100/($2+$4+$5)} END {print usage \"%\"}'", function (error, stdout, stderr) {
        if (error !== null) {
            throw error;
        }
        data.cpu = stdout.trim();

        exec("df -h | grep rootfs | awk '{print $5}'", function (error, stdout, stderr) {
            if (error !== null) {
                throw error;
            }
            data.memory = stdout.trim();

            callback(data);
        });
    });
};